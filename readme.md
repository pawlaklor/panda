# Intro
This README is an alpha version.

## Development

### Required
Virtualbox https://www.virtualbox.org/
Vagrant https://www.vagrantup.com
Composer
node.js

Useful:
SourceTree [for git]
CKDer [console, http://cmder.net/]
PHPStorm [editor]
Navicat / MySql Workbench [for database]

## Repository
- production: **master**
- staging: **develop** (we work generally on this branch!)

## Running virtual machine
Enter to appropriate directory. To run vagrant (virtual machine) type:
```
vagrant up
```

Turn off:
```
vagrant halt
```

Restart virtual machine (removing database!):
```
vagrant provision
```

Log into virtual machine
```
vagrant ssh
```

To exit type **exit**.

## Dependencies
After deployment on server you have to install dependencies:

### Composer

User composer on [vagrant] virtual machine.

Do all activity in the project directory:

```
cd /var/www/odkryjwroclaw
```

For the FIRST time on your machine type:
```
sudo composer self-update
```

To install all needed dependencies:
```
sudo composer install
```

You will see directory **vendor**.

### NPM

```
npm install
```

You will see directory **node_modules**.

### Bower

```
bower install
```

You will see directory **bower_components**.

## .ENV

File **.env** does not exist in repositorium. You have to create it. You can copy example from **.env.example** and edit it a little bit.

## Database

Create lacking migrations
```
php artisan migrate
```

Create specific migration file:
```
php artisan make:migration create_users_table
```

Rollback last migration:
```
php artisan migrate:rollback
```

## Gulp

To compile and watch just type:

```
gulp
```

You have to do it if any .less / .js files have been created/edited.

## Seeding
You can create fake data to database.

Run seeds:
```
php artisan db:seed
```

Run specific seed:
```
php artisan db:seed --class=UserTableSeeder
```

Rollback everything, run migration with seeds:
```
php artisan migrate:refresh --seed
```

## Cron

Here is the only Cron entry you need to add to your server:
```
* * * * * php /path/to/artisan schedule:run 1>> /dev/null 2>&1
```

## XDEBUG

Increase limit for number of class for xdebug in php settings
Usually is a file: **/etc/php5/cli/conf.d/20-xdebug.ini**

The parameter: **xdebug.max_nesting_level = 100** (increase 100 to e.g. 200)

## FOR UBUNTU (linux ?):

You need to have repository in **/var/www/html/** directory.

```
sudo apt-get install apache2 php5-cli curl php5-curl php5-mcrypt
sudo apt-get install libapache2-mod-php5
sudo a2enmod rewrite
```

```
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
```

in **/var/www/panda**:
```
sudo composer install
```
(you can skip token on phpunit, but better and faster wil be to create a token on your private github account)

You also can:
```
sudo apt-get install npm bower
```
```
sudo mv .env.example .env
```

edit **.env** (according to README.md)


edit **etc/hosts** file
```
localhost panda.dev
```

```
sudo touch /etc/apache2/sites-available/panda.dev.conf
```

edit this file:

```
<VirtualHost *:80>
    ServerName panda.dev
    ServerAlias *

    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/html/panda/public/

    ErrorLog ${APACHE_LOG_DIR}/panda.dev/error.log
    CustomLog ${APACHE_LOG_DIR}/panda.dev/access.log combined

    SetEnv ENV local
</VirtualHost>
```

Make sure:

```
<Directory /var/www/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
</Directory>
```

Especially: **AllowOverride All**
in **/etc/apache2/apache2.conf** or in your virtualhost.

```
sudo service apache2 restart
```
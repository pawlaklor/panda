<?php

/**
 * This array is mainly used by roles table seeder / permissions table seeder
 */

return [
    'roles' => [
        'ADMINISTRATOR' => [
            'display_name' => 'Administrator',
            'description' => 'High level administrator'
        ],
        'TEACHER' => [
            'display_name' => 'Teacher',
            'description' => 'Teacher'
        ],
        'STUDENT' => [
            'display_name' => 'Student',
            'description' => 'Student'
        ]
    ],
    'permissions' => []
];
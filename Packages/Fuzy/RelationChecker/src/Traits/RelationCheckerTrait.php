<?php namespace Fuzy\RelationChecker\Traits;

use Fuzy\RelationChecker\Exceptions\CantRemoveWithExistingRelationException;
use Fuzy\RelationChecker\Exceptions\ParentModelDoesNotContainChildrenException;

trait RelationCheckerTrait {

    // throw any Exception if there is no $protectedRelations array in model Class

    /**
     * Function checks based on model variable $protectedRelations if defined there relations contains any models, if yes,
     * this method will throw an exception with message that you cant remove this model
     *
     * @param      $id
     * @param null $relation
     * @return mixed
     * @throws CantRemoveWithExistingRelationException
     */
    public function checkRelations($id, $relation = null)
    {
        // Not same model as in Repository, this one actually have an id
        $model = $this->find($id);

        if ($relation) {

            if (count($model->{$relation}))
            {
                // Check if specified $relation is set in $protectedRelations variable
                if (isset($model->protectedRelations[$relation])) {
                    // We can throw error message from there
                    throw new CantRemoveWithExistingRelationException($model->protectedRelations[$relation]['message']);
                } else {
                    // Lets throw generic message
                    throw new CantRemoveWithExistingRelationException('You can\'t remove model with existing relations');
                }

            }
        } else {
            foreach ($model->protectedRelations as $relation => $data) {
                if (count($model->{$relation}))
                {
                    throw new CantRemoveWithExistingRelationException($data['message']);
                }
            }
        }

        return $model; // TODO: Figure out why returning $this is not working because i'm not sure

    }

    /**
     * This method will check if parent model contains $children_id model based on @relation
     *
     * @param      $children_id
     * @param      $relation
     * @param null $through
     * @return bool
     * @throws ParentModelDoesNotContainChildrenException
     */
    public function checkIfContains($children_id, $relation, $through = null)
    {
        if ($through) {
            if ($this->{$through}->{$relation}->contains($children_id)) {
                return true;
            } else {
                throw new ParentModelDoesNotContainChildrenException("Parent " . get_class($this) . ' does not contain children: ' . $children_id . ' with relation: ' . $relation . ' through: ' . $through);
            }
        } else {
            if ($this->{$relation}->contains($children_id)) {
                return true;
            } else {
                throw new ParentModelDoesNotContainChildrenException("Parent " . get_class($this) . ' does not contain children: ' . $children_id . ' with relation: ' . $relation);
            }
        }
    }
}
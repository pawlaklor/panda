<?php namespace Fuzy\RelationChecker\Exceptions;

use Exception;

class CantRemoveWithExistingRelationException extends Exception {
    
}
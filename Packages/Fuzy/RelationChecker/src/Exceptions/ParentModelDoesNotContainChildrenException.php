<?php namespace Fuzy\RelationChecker\Exceptions;

use Exception;

class ParentModelDoesNotContainChildrenException extends Exception {

}
## Relation Checker

### Usage:

in controller catch:
```
CantRemoveWithExistingRelationException.php
```

in almost main repository while deleting use:
```
return $this->model->checkRelations()->delete($id);
```

in model define variable with protected relations
```
public $protectedRelations = [
        'emails' => [
                'message' => 'Can not remove 1',
                'redirect' => [
                    'route'
                ]
            ],
        'IPs' => [
            'message' => 'Can not remove 2'
        ]
    ];
```

## TODO
* Redirect route for next update, something linek getRedirect() in AccessRepository
* Add way to check relation between two models with defined ids and relation name
<?php namespace Fuzy\Repository\Contracts;



interface RepositoryInterface {

    /**
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*'));

    /**
     * @return mixed
     * @internal param array $data
     */
    public function create();

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);

    /**
     * @param array  $data
     * @param        $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute = "id");

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*'));

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id);

}
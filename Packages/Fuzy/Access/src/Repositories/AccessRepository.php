<?php namespace Fuzy\Access\Repositories;


use Fuzy\Access\Exceptions\AccessException;
use Fuzy\Access\Exceptions\NotEnoughPermissionsException;
use Fuzy\Access\Exceptions\NotEnoughRolesException;
use Illuminate\Support\Facades\Auth;

/**
 * Class AccessRepository
 *
 * TODO Change the way of checking permissions maybe?
 * TODO Add ability to redirect to different routes for each function, not only whole ***AccessRepository
 *
 * @package Fuzy\Access\Repositories
 */
class AccessRepository {

    protected $access = [

    ];

    protected $currentUser;

    private $defaults = [
        'redirect' => [
            'route' => 'admin.dashboard'
        ]
    ];

    protected $function;

    /**
     *
     */
    public function _construct(){
        $this->currentUser = AUTH::user();
    }

    /**
     * Example of another method for checking access
     */
    public function passesAjax() {
        // $this->init();
        // $this->checkRoles();
    }

    /**
     * And another
     */
    public function passesRoles() {
        // $this->init();
        // $this->checkRoles();
    }

    /**
     * One of methods of checking access
     *
     * @return bool
     * @throws NotEnoughPermissionsException
     * @throws NotEnoughRolesException
     */
    public function passes() {
        $this->init();

        // TODO Change to checkRoles() and checkPermissions() separate functions
        $this->runAccessChecker();
    }

    private function runAccessChecker()
    {
        // TODO Function should return if access is granted after checking variable in object that have some information about access, dont know what exactly
        // TODO Not finished!

        // TODO Make it work with $this->access array so it will check if request? or something comes from specified function and check proper access accordingly
        // TODO Throwing exception if $passes array is properly formatted


        // If function exist in $passes array then lets foreach through roles and permissions
        if (isset($this->access['functions'][$this->function])) {

            // Check if 'roles' array index is set
            if (isset($this->access['functions'][$this->function]['roles'])) {
                // Foreach through roles, if any then go on
                foreach($this->access['functions'][$this->function]['roles'] as $role) {
                    if (!Auth::user()->hasRole($role)) {

                        throw new NotEnoughRolesException('Missing role: '. $role);

                    }
                }
            }

            // Check if 'permissions' array index is set
            if (isset($this->access['functions'][$this->function]['permissions'])) {
                // Foreach through permissions, if any then go on
                foreach($this->access['functions'][$this->function]['permissions'] as $permission) {
                    if (!Auth::user()->can($permission)) {

                        throw new NotEnoughPermissionsException('Missing permission: '.$permission);

                    }
                }
            }

            // Foreach through anything else we would like to include there, if any then go on

        }

        return true;


    }

    /**
     * Function to based on backtrace set function that was calling
     *
     * Don't know if always 2nd element of backtrace will be function that was calling, need to be monitored
     */
    private function setFunction()
    {
        // TODO Add checking if actual function name is in 5th element of array, or make it work differently

        // Careful here with 5th element of array, if structure of whole AccessRepository and AccessRepositoryRepository
        // will change this number will have to be updated too
        return $this->function = debug_backtrace()[4]['function'];
    }

    /**
     * Method used in controllers to check if access is allowed
     *
     * @throws AccessException
     */
    public function checkAccess() {

        try {

            $this->passes();

        } catch (NotEnoughPermissionsException $e) {

            // TODO Think about place to display error messages, FLASH or sth

            throw new AccessException('You don\'t have enough power to do that.');

        } catch (NotEnoughRolesException $e) {

            throw new AccessException('You don\'t have enough power to do that.');

        }
    }

    public function getRedirect()
    {
        // TODO Read TODO at the top of this file

        if (isset($this->access['redirect']['route'])) {
            return redirect()->route($this->access['redirect']['route']);
        } else {
            return redirect()->route($this->defaults['redirect']['route']);
        }
    }

    private function init()
    {
        // Get function name that is calling and store it in object variable
        $this->setFunction();
    }
}
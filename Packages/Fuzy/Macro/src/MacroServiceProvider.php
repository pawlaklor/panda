<?php namespace Fuzy\Macro;

use Form;
use HTML;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;

class MacroServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->registerFormMacros();
        $this->registerHtmlMacros();
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

    private function registerHtmlMacros()
    {
        HTML::macro('linkTel', function($number, $title = null, $attributes = [], $secure = null)
        {
            $numberTel = preg_replace('/\s+/', '', $number);

            return HTML::link("tel:{$numberTel}", $title ?: $number, $attributes, $secure);
        });
    }

    private function registerFormMacros()
    {
        Form::macro('error', function($field) {
            $errors = Session::get('errors');

            if($errors && $errors->has($field)) {
                return '<div class="text-danger small">'.$errors->first($field).'</div>';
            }

            return false;
        });

        Form::macro('group', function($field, Callable $content) {
            $html = [];
            $errors = Session::get('errors');

            if($errors && $errors->has($field)) {
                array_push($html, '<div class="form-group has-error">');
            }
            else {
                array_push($html, '<div class="form-group">');
            }

            array_push($html, $content());
            array_push($html, '</div>');

            return implode('', $html);

        });

        Form::macro('openGroup', function ($field, $additionalClasses = null) {
            $errors = Session::get('errors');

            if ($errors && $errors->has($field)) {
                return '<div class="form-group has-error ' . ($additionalClasses ? ' ' . $additionalClasses  : '' ). '">';

            }

            return '<div class="form-group ' . ($additionalClasses ? $additionalClasses : '' ). '">';
        });

        Form::macro('openGroupWithOutClass', function ($field) {
            $errors = Session::get('errors');

            if ($errors && $errors->has($field)) {
                return '<div class="has-error">';
            }

            return '<div class="">';
        });

        Form::macro('closeGroup', function() {
            return '</div>';
        });

        Form::macro('help', function($text) {
            return '<div class="help-block">'.$text.'</div>';
        });

    }

}

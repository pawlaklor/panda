var gulp = require('gulp'),
    browserfiy = require('gulp-browserify'),
    rename = require('gulp-rename'),
    livereload = require('gulp-livereload'),
    less = require('gulp-less'),
    uglify = require('gulp-uglify'),
    notify = require('gulp-notify'),
    debowerify = require('debowerify'),
    minifyCSS = require('gulp-minify-css'),
    autoprefixer = require('gulp-autoprefixer');

var settings = {
    styles: {
        1: {
            src: './resources/assets/less/app.less',
            files: './resources/assets/less',
            dest: './public/assets/css',
            filename: 'app.min.css',
            filename_min: 'app.min.css'
        },
        length: 1,
        task: 'less',
        search_here: './resources/assets/less/**/**/*.less'
    },
    scripts: {
        1: {
            src: './resources/assets/js/app.js',
            files: './resources/assets/js',
            dest: './public/assets/js',
            filename: 'app.min.js',
            filename_min: 'app.min.js'
        },
        length: 1,
        task: 'js',
        search_here: './resources/assets/js/**/**/*.js'
    },
    copy: {
        1: {
            from: './bower_components/bootstrap/fonts/*.*',
            to: './public/assets/fonts/'
        },
        2: {
            from: './bower_components/components-font-awesome/fonts/*.*',
            to: './public/assets/fonts/'
        },
        /**
         * Datatables
         */
        3: {
            from: './bower_components/datatables/media/js/jquery.dataTables.min.js',
            to: './public/assets/js/'
        },
        4: {
            from: './bower_components/datatables/media/js/dataTables.bootstrap.min.js',
            to: './public/assets/js/'
        },
        // End Datatables
        length: 4,
        task: 'copy'
    }
}

gulp.task(settings.scripts.task, function () {
    for (var i = settings.scripts.length; i > 0; i--) {
        gulp.src(settings.scripts[i].src)
            .pipe(browserfiy({
                transform: ['debowerify'],
                insertGlobals: false,
                debug: true
            }))
            .on('error', notify.onError({
                title: 'JS!',
                message: '<%= error.message %>'
            }))
            .pipe(rename(settings.scripts[i].filename))
            .pipe(gulp.dest(settings.scripts[i].dest))
    };
});

/* TODO: Write init procedure */
gulp.task(settings.copy.task, function () {
    for (var i = settings.copy.length; i > 0; i--) {
        gulp.src(settings.copy[i].from)
            .pipe(gulp.dest(settings.copy[i].to))
    };
});

gulp.task(settings.styles.task, function () {
    for (var i = settings.styles.length; i > 0; i--) {
        gulp.src(settings.styles[i].src)
            .pipe(less({
                outputStyle: 'compressed',
                sourceComments: 'map',
                includePaths: [settings.styles[i].files]
            }))
            .on('error', notify.onError({
                title: settings.styles.task,
                message: '<%= error.message %>'
            }))
            .pipe(rename(settings.styles[i].filename))
            .pipe(gulp.dest(settings.styles[i].dest))
    };
});

gulp.task('watch', function () {
    livereload.listen();
    gulp.watch(settings.scripts.search_here, [settings.scripts.task]).on('change', livereload.changed);
    gulp.watch(settings.styles.search_here, [settings.styles.task]).on('change', livereload.changed);
});

gulp.task('release', function() {
    // release scripts
    for (var i = settings.scripts.length; i > 0; i--) {
        gulp.src(settings.scripts[i].src)
            .pipe(browserfiy({
                transform: ['debowerify'],
                insertGlobals: false,
                debug: true
            }))
            .pipe(uglify())
            .pipe(rename(settings.scripts[i].filename_min))
            .pipe(gulp.dest(settings.scripts[i].dest));
    };

    // release styles
    for (var i = settings.styles.length; i > 0; i--) {
        gulp.src(settings.styles[i].src)
            .pipe(autoprefixer(
                'last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'
            ))
            .pipe(minifyCSS())
            .pipe(rename(settings.styles[i].filename_min))
            .pipe(gulp.dest(settings.styles[i].dest))
    };
});

gulp.task('default', [settings.scripts.task, settings.styles.task, settings.copy.task, 'watch']);
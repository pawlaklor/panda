#!/usr/bin/env bash

block="<VirtualHost *:80>
    ServerName $1
    ServerAlias *

    ServerAdmin webmaster@localhost
    DocumentRoot $2

    ErrorLog \${APACHE_LOG_DIR}/$1/error.log
    CustomLog \${APACHE_LOG_DIR}/$1/access.log combined

    SetEnv ENV local
</VirtualHost>
"

sudo mkdir -p "/var/log/apache2/$1"
sudo echo "$block" > "/etc/apache2/sites-available/$1.conf"
sudo ln -fs "/etc/apache2/sites-available/$1.conf" "/etc/apache2/sites-enabled/$1.conf"
sudo service apache2 restart

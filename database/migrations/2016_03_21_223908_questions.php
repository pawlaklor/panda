<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Questions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_type_id')->unsigned()->nullable();
            $table->text('text');
            $table->integer('points');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('questions', function (Blueprint $table) {
            $table->foreign('question_type_id')->references('id')->on('questions');
        });

        Schema::create('test_question', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('test_id')->unsigned()->nullable();
            $table->integer('question_id')->unsigned()->nullable();
            $table->integer('question_type_id')->unsigned()->nullable();
        });

        Schema::table('test_question', function (Blueprint $table) {
            $table->foreign('test_id')->references('id')->on('tests');
            $table->foreign('question_id')->references('id')->on('questions');
            $table->foreign('question_type_id')->references('id')->on('question_types');
        });

        Schema::create('question_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_id')->unsigned()->nullable();
            $table->text('text');
            $table->integer('correct');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('question_options', function (Blueprint $table) {
            $table->foreign('question_id')->references('id')->on('questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('question_options', function(Blueprint $table) {
            $table->dropForeign('question_options_question_id_foreign');
        });

        Schema::drop('question_options');

        Schema::table('test_question', function(Blueprint $table) {
            $table->dropForeign('test_question_test_id_foreign');
            $table->dropForeign('test_question_question_id_foreign');
            $table->dropForeign('test_question_question_type_id_foreign');
        });

        Schema::drop('test_question');

        Schema::table('questions', function(Blueprint $table) {
            $table->dropForeign('questions_question_type_id_foreign');
        });

        Schema::drop('questions');

        Schema::drop('question_types');
    }
}

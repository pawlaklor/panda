<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Courses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('password');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('course_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
        });

        Schema::table('course_user', function(Blueprint $table){
            $table->foreign('course_id')->references('id')->on('courses');
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('materials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id')->unsigned()->nullable();
            $table->string('name', 100);
            $table->string('path', 255);
            $table->timestamps();
        });

        Schema::table('materials', function(Blueprint $table){
            $table->foreign('course_id')->references('id')->on('courses');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('materials', function(Blueprint $table) {
            $table->dropForeign('materials_course_id_foreign');
        });

        Schema::drop('materials');

        Schema::table('course_user', function(Blueprint $table) {
            $table->dropForeign('course_user_course_id_foreign');
            $table->dropForeign('course_user_user_id_foreign');
        });

        Schema::drop('course_user');

        Schema::drop('courses');
    }
}

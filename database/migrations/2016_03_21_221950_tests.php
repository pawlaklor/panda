<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prev_test_id')->unsigned()->nullable();
            $table->string('name', 255);
            $table->integer('duration');
            $table->integer('total_points');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('tests', function (Blueprint $table) {
            $table->foreign('prev_test_id')->references('id')->on('tests');
        });

        Schema::create('test_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('test_id')->unsigned()->nullable();
            $table->integer('points');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('test_results', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('test_id')->references('id')->on('tests');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('test_results', function(Blueprint $table) {
            $table->dropForeign('test_results_user_id_foreign');
            $table->dropForeign('test_results_test_id_foreign');
        });

        Schema::drop('test_results');

        Schema::table('tests', function(Blueprint $table) {
            $table->dropForeign('tests_prev_test_id_foreign');
        });

        Schema::drop('tests');
    }
}

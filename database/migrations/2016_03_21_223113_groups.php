<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Groups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id')->unsigned()->nullable();
            $table->string('place', 255);
            $table->timestamp('datetime');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('groups', function (Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses');
        });

        Schema::create('test_group', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('test_id')->unsigned()->nullable();
            $table->integer('group_id')->unsigned()->nullable();
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->string('password', 255);
            $table->timestamps();
        });

        Schema::table('test_group', function (Blueprint $table) {
            $table->foreign('test_id')->references('id')->on('tests');
            $table->foreign('group_id')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('test_group', function(Blueprint $table) {
            $table->dropForeign('test_group_test_id_foreign');
            $table->dropForeign('test_group_group_id_foreign');
        });

        Schema::drop('test_group');

        Schema::table('groups', function(Blueprint $table) {
            $table->dropForeign('groups_course_id_foreign');
        });

        Schema::drop('groups');
    }
}

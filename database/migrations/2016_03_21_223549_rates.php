<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Rates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('test_id')->unsigned()->nullable();
            $table->integer('mark3');
            $table->integer('mark35');
            $table->integer('mark4');
            $table->integer('mark45');
            $table->integer('mark5');
            $table->integer('mark55');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('rates', function (Blueprint $table) {
            $table->foreign('test_id')->references('id')->on('tests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rates', function(Blueprint $table) {
            $table->dropForeign('rates_test_id_foreign');
        });

        Schema::drop('rates');
    }
}

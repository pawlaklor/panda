<?php

use App\Users\Role;

class RolesTableSeeder extends DatabaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (config('roles.roles') as $k => $v) {
            if ( ! Role::whereName($k)->get()->first()) {
                $role = new Role();
                $role->name         = $k;
                $role->display_name = ($v['display_name']) ? $v['display_name'] : '';
                $role->description  = ($v['description']) ? $v['description'] : '';
                $role->save();
            } else {
                $this->command->info($k . ' role already exists.');
            }
        }
    }
}

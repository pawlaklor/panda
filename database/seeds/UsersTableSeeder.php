<?php

use App\Users\Role;

class UsersTableSeeder extends DatabaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createAdminUser();
        $this->createTeacher();

        // Create users associated with institutions:
        for ($i = 0; $i <= 30; $i++) {
            $user = new \App\Users\User;
            $user->name = $this->faker->firstName;
            $user->surname = $this->faker->lastName;
            $user->email = $this->faker->email;
            $user->password = 'secret';
            $user->verified = 1;

            $user->save();

            $student = Role::where('name', 'STUDENT')->first();
            $user->roles()->attach($student);
        }
    }

    /**
     * @return \App\Users\User
     */
    private function createAdminUser()
    {
        if (!\App\Users\User::where('email', 'admin@admin.com')->count())
        {
            $user = new \App\Users\User;
            $user->name = 'Super';
            $user->surname = 'Admin';
            $user->email = 'admin@admin.com';
            $user->password = 'secret'; // change because hashing password is performed during user creation in User.php model
            $user->verified = 1;
            $user->save();

            $admin = Role::where('name', 'ADMINISTRATOR')->first();
            $user->roles()->attach($admin);


        }
        else
        {
            $this->command->info('Admin user already exists.');;
        }
    }

    private function createTeacher()
    {
        if (!\App\Users\User::where('email', 'teacher@admin.com')->count())
        {
            $user = new \App\Users\User;
            $user->name = 'Jan';
            $user->surname = 'Nikodem';
            $user->email = 'teacher@admin.com';
            $user->password = 'secret'; // change because hashing password is performed during user creation in User.php model
            $user->verified = 1;
            $user->save();

            $teacher = Role::where('name', 'TEACHER')->first();
            $user->roles()->attach($teacher);
        }
        else
        {
            $this->command->info('Teacher already exists.');;
        }
    }
}

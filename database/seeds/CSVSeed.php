<?php

use App\Users\Role;
use App\Users\User;

class CSVSeed extends DatabaseSeeder
{
    /**
     * Run the database seeds.
     * Reads data from scv file
     * @return void
     */
    public function run()
    {
        $filename = "database/seeds/INES00309P_C_7_30_E04-42a.csv";
        $cutoff = 0;
        $fd = fopen($filename, "r") or die("Unable to open file!");
        $content = fread($fd,filesize($filename));
        fclose($fd);
        $pieces = explode(PHP_EOL, $content); //Define EOL \r\n or just \n
        for($i=0; $i < count($pieces); $i++)
        {
            if($cutoff == 1)
            {
                $customer = str_getcsv($pieces[$i], ';');

                $surname = iconv("cp1250","UTF-8",$customer[2]);
                $names = iconv("cp1250","UTF-8",$customer[3]);
                $names = explode(' ', $names);
                $first_name = $names[0];

                $indexNumberLong = utf8_encode($customer[1]);
                $indexNumber = substr($indexNumberLong, 4, 6);
                $email = $indexNumber.'@student.pwr.edu.pl';
                
                $user = new User;
                $user->name =  $first_name;
                $user->surname = $surname;
                $user->email = $email;
                $user->password = 'secret';
                $user->verified = 1;
                $user->save();

                $student = Role::where('name', 'STUDENT')->first();
                $user->roles()->attach($student);

            } else{
                if(strpos($pieces[$i],"Nr albumu") !== FALSE)
                {
                    $cutoff = 1;
                }
            }
        }
    }

}

# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

require 'yaml'

path = "#{File.dirname(__FILE__)}"
settings = YAML::load(File.read(path + '/machine/machine.yaml'));


Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

    # Configure The Box
    config.vm.box = "fuzy/ubuntu64"
    config.vm.hostname = settings["hostname"] ||= "ubuntu64"
    config.vm.box_check_update = false

    # Configure A Private Network IP
    config.vm.network :private_network, ip: settings["ip"] ||= "192.168.55.101"

    # Configure A Few VirtualBox Settings
    config.vm.provider "virtualbox" do |vb|
        vb.customize ["modifyvm", :id, "--memory", settings["memory"] ||= "1024"]
        vb.customize ["modifyvm", :id, "--cpus", settings["cpus"] ||= "1"]
        vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    end

    # Configure Port Forwarding To The Box
    settings["forward"].each do |port|
        config.vm.network "forwarded_port", guest: port["guest"], host: port["host"], id: port["id"], auto_correct: true
    end

    # Register All Of The Configured Shared Folders
    settings["folders"].each do |folder|
        config.vm.synced_folder folder["map"], folder["to"], type: folder["type"] ||= nil
    end

    # Create databases and users
    settings["databases"].each do |db|
        config.vm.provision "shell" do |s|
            s.inline = "mysql -uroot -pvagrant -e \"drop database if exists $1\" 2>/dev/null"
            s.args = [db["name"]]
        end

        config.vm.provision "shell" do |s|
            s.inline = "mysql -uroot -pvagrant -e \"create database $1\" 2>/dev/null"
            s.args = [db["name"]]
        end

        config.vm.provision "shell" do |s|
            s.inline = "mysql -uroot -pvagrant -e \"GRANT ALL PRIVILEGES ON $1.* TO $2@localhost IDENTIFIED BY '$3'  WITH GRANT OPTION\" 2>/dev/null"
            s.args = [db["name"], db['user'], db['password']]
        end
    end

    settings["sites"].each do |site|
        config.vm.provision "shell" do |s|
            s.inline = "sudo bash /vagrant/machine/serve.sh $1 $2"
            s.args = [site["map"], site["to"]]
        end
    end
end

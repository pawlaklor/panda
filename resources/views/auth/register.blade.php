@extends('layouts.guest.main', ['body_classes' => 'body-bg'])

@section('content')
    @include('layouts.guest.header')


    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-md-12">

                {!! Form::open(['url' => '/register', 'class' => 'form-front']) !!}

                {!! Form::openGroup('name') !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'required', 'placeholder' => 'First Name']) !!}
                {!! Form::error('name') !!}
                {!! Form::closeGroup() !!}

                {!! Form::openGroup('surname') !!}
                {!! Form::text('surname', null, ['class' => 'form-control', 'required', 'placeholder' => 'Last Name']) !!}
                {!! Form::error('surname') !!}
                {!! Form::closeGroup() !!}

                {!! Form::openGroup('email') !!}
                {!! Form::text('email', null, ['class' => 'form-control', 'required', 'placeholder' => 'E-mail address']) !!}
                {!! Form::error('email') !!}
                {!! Form::closeGroup() !!}

                {!! Form::openGroup('password') !!}
                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
                {!! Form::error('password') !!}
                {!! Form::closeGroup() !!}

                {!! Form::openGroup('password_confirmation') !!}
                {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm Password']) !!}
                {!! Form::error('password_confirmation') !!}
                {!! Form::closeGroup() !!}


                <div class="form-group text-right">
                    <button type="submit" class="btn btn-lg btn-primary submit">Register</button>
                </div>


                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@if (session('status'))
    {{ session('status') }}
@endif
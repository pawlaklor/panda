@extends('layouts.guest.main', ['body_classes' => 'body-bg'])

@section('styles')
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,400italic,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
@endsection

@section('scripts')
    <!-- Extra script from blade -->
@endsection


@section('content')
@include('layouts.guest.header')

    <div class="container">

        <div class="row">
            <div class="col-xs-12 col-md-12">

                {!! Form::open(['url' => '/login', 'class' => 'form-front']) !!}

                    {!! Form::openGroup('email') !!}
                        {!! Form::text('email', null, ['class' => 'form-control', 'required', 'placeholder' => 'E-Mail']) !!}
                        {!! Form::error('email') !!}
                    {!! Form::closeGroup() !!}

                    {!! Form::openGroup('password') !!}
                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
                        {!! Form::error('password') !!}
                    {!! Form::closeGroup() !!}

                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-lg btn-primary submit">Login</button>
                    </div>

                    <a href="{{ url('/password/email') }}" class="pull-left">Forgotten password</a>
                    <a href="{{ url('/register') }}" class="pull-right">Register</a>

                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
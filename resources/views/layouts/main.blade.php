@extends('layouts.html', ['body_classes' => 'default-bg'])

@section('body')

    @include("parts.header")

    @yield('content')

    @include('parts.footer')
@stop

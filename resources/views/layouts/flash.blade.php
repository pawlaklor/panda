@if(Session::has('flash.alerts'))
    @foreach(Session::get('flash.alerts') as $alert)
        <div style="display: none;" class="toast" data-toast-level="{{ $alert['level'] }}" @if(!empty($alert['title'])) data-toast-title="{{ $alert['title'] }}" @endif data-toast-message="{{ $alert['message'] }}"></div>
    @endforeach
@endif
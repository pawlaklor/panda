<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <ul class="he-footer">
                <li>
                    <li><a href="">Terms & conditions</a></li></li>
                    <li><a href="">Privacy Policy</a></li></li>
                    <li><a href="">Contact us</a></li>
                </li>
            </ul>

            <div class="pull-right copyright">Copyright &copy; {!! date('Y') !!} Panda. All rights reserved.</div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
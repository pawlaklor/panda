<header>
    <div class="container-fluid navy-bg">
        <div class="row">
            <div class="container">
                <div class="row">

                    <div class="header__icons pull-right">
                        <div class="header__icons-icon">
                            <a href="#">
                                <p>Help</p>
                            </a>
                        </div>

                        <div class="header__icons-icon">
                            <a href="{{ url('logout') }}">
                                <img src="/assets/images/logout.png">

                                <p>Logout</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid white-bg">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="header__submenu">
                        <nav class="navbar navbar-default">
                            <div class="container-fluid">
                                <div class="row">
                                    <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>

                                        <ul class="nav navbar-nav">

                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="/assets/images/burger.png"></a>
                                                <ul class="dropdown-menu">
                                                    {{--Need check the permission here!--}}

                                                    <li><a href="{{ route('users.index') }}">Użytkownicy</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                        <ul class="nav navbar-nav">

                                            <li><a href="/"><img src="/assets/images/home.png"></a></li>

                                        </ul>

                                        {{--Right site--}}

                                        <ul class="nav navbar-nav navbar-right">
                                            <li class="header__username"><a href="#">Name</a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="/assets/images/profile-settings.png"></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Edit name</a></li>
                                                    <li><a href="#">Change password</a></li>
                                                    <li><a href="#">Add user</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#">Send message to support</a></li>
                                                </ul>
                                            </li>
                                        </ul>

                                    </div>
                                    <!-- /.navbar-collapse -->
                                </div>
                                <!-- /.container-fluid -->
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
window.$ = window.jQuery = require('jquery');

window.CKEDITOR_BASEPATH = '/vendor/ckeditor/';

require('bootstrap');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


/**
 * Lets load some modules that will prepare output
 * e.g.
 * date pickers,
 * datetime pickers,
 * wysiwyg editors -> CKE
 */
require('./modules/cke.js');
require('./modules/confirm.js');
require('./modules/toastr.js');
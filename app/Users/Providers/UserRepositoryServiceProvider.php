<?php namespace App\Users\Providers;

use App\Roles\Contracts\RoleRepositoryInterface;
use App\Roles\Repositories\AccessRoleRepository;
use App\Roles\Repositories\RoleRepository;
use App\Users\Repositories\AccessUserRepository;
use App\Users\Repositories\LoggingUserRepository;
use App\Users\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

use Log;

class UserRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->singleton(AccessUserRepository::class, function() {

//            $roleRepository = new RoleRepository(); // TODO: Don't know if that's entirely good idea

            $userRepository = new UserRepository();

//            $firstAccessUserRepository = new FirstAccessUs2erRepository($userRepository, app()->make(Auth::class));
            $loggingUserRepository = new LoggingUserRepository($userRepository, $this->app['log']);

//            $cachingUserRepository = new CachingUserRepository($loggingUserRepository, $this->app['cache.store']);

            return $accessUserRepository = new AccessUserRepository($loggingUserRepository);

        });
    }
}

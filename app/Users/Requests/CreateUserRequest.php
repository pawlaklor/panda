<?php namespace App\Users\Requests;

use App\Http\Requests\Request;

class CreateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'email' => 'required|email|max:255|unique:users',
            'job_title' => 'required|max:100',
            'institution_id' => 'required|exists:institutions,id',
            'allow_local_agreements' => 'required',
            'password' => 'required|confirmed|min:6',
        ];
    }

    public function messages() {
        return [];
    }

}

<?php namespace App\Users\Requests;

use App\Http\Requests\Request;

class UpdateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = $this->route()->getParameter('id');

        $rules = [
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'email' => "required|email|max:255|unique:users,email,{$user_id}",
            'job_title' => 'required|max:100',
            'institution_id' => 'required|exists:institutions,id',
            'allow_local_agreements' => 'required',
            'password' => 'confirmed|min:6',
        ];

        if ($this->request->get('roles')) {
            foreach($this->request->get('roles') as $key => $val)
            {
                $rules['roles.'.$key] = 'exists:roles,id';
            }
        }

        return $rules;
    }

    public function messages() {
        return [];
    }

}

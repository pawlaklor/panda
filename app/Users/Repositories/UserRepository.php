<?php namespace App\Users\Repositories;

use App\Roles\Contracts\RoleRepositoryInterface;
use App\Users\User;
use App\Users\Contracts\UserRepositoryInterface;
use Fuzy\Repository\Contracts\RepositoryInterface;
use Fuzy\Repository\Repository;
use Illuminate\Support\Facades\App;

class UserRepository extends Repository implements UserRepositoryInterface, RepositoryInterface {

    public function __construct()
    {
        parent::__construct();

    }
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return User::class;
    }

    public function getByInstitution($institution)
    {
        return $this->model->where('institution_id', $institution->id);
    }

    public function store(array $data)
    {
        return $this->model->create($data); // before: User::
    }

    public function update(array $data, $id, $attribute = "id") {

        if (isset($data['password'])) {
            $data['password'] = bcrypt($data['password']);
        }


        $data = $this->updateRoles($id, $data); // This may cause problems when update will be trigged from other place than user edit page, from somewhere where we don't want / can't change roles

        return $this->model->where($attribute, '=', $id)->update($data);
    }

    public function approveRegistration($token)
    {
        // Careful here, we are assuming that there is no possibility of creating two same registration_token
        return $this->model->where('registration_token', $token)->firstOrFail()->approveRegistration();
    }

    public function declineRegistration($token)
    {
        // Careful here, we are assuming that there is no possibility of creating two same registration_token
        return $this->model->where('registration_token', $token)->firstOrFail()->declineRegistration();
    }

    public function confirmEmail($token)
    {
        // Careful here, we are assuming that there is no possibility of creating two same registration_token
        return $this->model->where('registration_token', $token)->firstOrFail()->confirmEmail();
    }

    public function verify(User $user)
    {
        return $this->model->find($user->id)->verify();
    }

    private function updateRoles($id, $data)
    {
        // Check if any roles came with request, if none just make it an empty array
        $data['roles'] = isset($data['roles']) ? $data['roles'] : [];

        $user = $this->model->find($id);

        $roleRepository = App::make(RoleRepositoryInterface::class);

        $roleRepository->syncUserRoles($user, $data);

        // Unfortunately cant use dependency injection, some problems when creating __construct here, need to figure you why it happens
        // $this->role->syncUserRoles($user, $data);

        // After all work is done, remove 'roles' key in array, so update can proceed freely
        return $data = array_except($data, 'roles');


    }
}
<?php namespace App\Users\Repositories;

use App\Users\Contracts\UserRepositoryInterface;
use App\Users\User;
use Fuzy\Repository\Contracts\RepositoryInterface;

class CachingUserRepository implements UserRepositoryInterface, RepositoryInterface {

    /**
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*'))
    {
        // TODO: Implement all() method.
    }

    /**
     * @param       $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 1, $columns = array('*'))
    {
        // TODO: Implement paginate() method.
    }

    /**
     * @return mixed
     * @internal param array $data
     */
    public function create()
    {
        // TODO: Implement create() method.
    }

    /**
     * @param array  $data
     * @param        $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute = "id")
    {
        // TODO: Implement update() method.
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param       $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*'))
    {
        // TODO: Implement find() method.
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        // TODO: Implement store() method.
    }

    public function getByInstitution($institution)
    {
        // TODO: Implement getByInstitution() method.
    }

    public function approveRegistration($token)
    {
        // TODO: Implement approveRegistration() method.
    }

    public function declineRegistration($token)
    {
        // TODO: Implement declineRegistration() method.
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        // TODO: Implement edit() method.
    }

    public function verify(User $user)
    {
        // TODO: Implement verify() method.
    }
}
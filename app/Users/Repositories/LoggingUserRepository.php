<?php namespace App\Users\Repositories;

use App\Users\Contracts\UserRepositoryInterface;
use App\Users\User;
use Fuzy\Repository\Contracts\RepositoryInterface;
use Illuminate\Contracts\Logging\Log;

// TODO: Create LoggingRepository for global logging / reporting system


class LoggingUserRepository implements UserRepositoryInterface, RepositoryInterface {

    /**
     * @var UserRepository
     */
    private $user;
    /**
     * @var Log
     */
    private $log;

    public function __construct(UserRepository $user, Log $log)
    {
        $this->user = $user;
        $this->log = $log;
    }



    /**
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*'))
    {
        \Log::info('Someone tried to call all() function in UserRepository');

        return $this->user->all($columns);
    }

    /**
     * @return mixed
     * @internal param array $data
     */
    public function create()
    {
        return $this->user->create();
        // TODO: Implement create() method.
    }

    /**
     * @param array  $data
     * @param        $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute = "id")
    {
        return $this->user->update($data, $id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param       $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*'))
    {
        return $this->user->find($id);
    }

    public function getByInstitution($institution)
    {
        return $this->user->getByInstitution($institution);
        // TODO: Implement getByInstitution() method.
    }

    public function approveRegistration($token)
    {
        return $this->user->approveRegistration($token);
    }

    public function declineRegistration($token)
    {
        return $this->user->declineRegistration($token);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->user->store($data);
    }

    public function verify(User $user)
    {
        return $this->user->verify($user);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->user->edit($id);
    }

    public function getRoles(User $user)
    {
        return $this->user->getRoles($user);
    }
}
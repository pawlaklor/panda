<?php namespace App\Users\Repositories;

use App\Users\Contracts\Institution;
use App\Users\Contracts\UserRepositoryInterface;
use App\Users\User;
use Fuzy\Access\Exceptions\AccessException;
use Fuzy\Access\Exceptions\NotEnoughPermissionsException;
use Fuzy\Access\Exceptions\NotEnoughRolesException;
use Fuzy\Access\Repositories\AccessRepository;
use Fuzy\Repository\Contracts\RepositoryInterface;

class AccessUserRepository extends AccessRepository implements RepositoryInterface, UserRepositoryInterface {

    /**
     * @var LoggingUserRepository
     */
    private $user;

    protected $access = [

        'functions' => [
            'all' => [
                'roles' => [],
                'permissions' => []
            ]
        ],
        'redirect' => [
            'route' => 'admin.dashboard'
        ]
    ];

    public function __construct(LoggingUserRepository $user) {

        $this->user = $user;
    }

    /**
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*'))
    {
        $this->checkAccess();
        return $this->user->all($columns);
    }


    /**
     * @return mixed
     * @internal param array $data
     */
    public function create()
    {
        $this->checkAccess();
    }

    /**
     * @param array  $data
     * @param        $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute = "id")
    {
        // TODO: Add permissions
        return $this->user->update($data, $id);
        // TODO: Implement update() method.
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param       $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*'))
    {
        return $this->user->find($id);
    }

    public function getByInstitution($institution)
    {
        var_dump('AccessUserRepository');
        return $this->user->getByInstitution($institution);
        // TODO: Implement getByInstitutions() method.
    }

    public function approveRegistration($token)
    {
        // TODO ADD PERMISSIONS AND ROLE FOR THIS

        return $this->user->approveRegistration($token);
    }

    public function declineRegistration($token)
    {
        // TODO ADD PERMISSIONS AND ROLE FOR THIS

        return $this->user->declineRegistration($token);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        // TODO: Add permissions!
        $this->checkAccess();
        return $this->user->store($data);
    }

    public function verify(User $user)
    {
        // TODO: Add permissions! I think this permission should be same as store() above ^
        $this->checkAccess();

        return $this->user->verify($user);
    }

    public function edit($id)
    {
        return $this->user->edit($id);
    }

    public function getRoles(User $user)
    {
        // TODO: Add permissions for users who can retrieve other users roles
        return $this->user->getRoles($user);
    }
}
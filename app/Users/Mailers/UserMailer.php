<?php namespace App\Users\Mailers;

use App\Users\User;
use Fuzy\Mailer\HtmlMailer;

class UserMailer extends HtmlMailer {

    public function sendEmailWithUsernameAndPasswordAfterCreationTo(User $user, $password)
    {
        $subject = "HE Contracts - User account created";
        $view = "backend.users.emails.email-after-creation";

        return $this->send($user->email, $subject, $view, compact('user', 'password'));
    }
}
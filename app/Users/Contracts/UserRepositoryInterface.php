<?php namespace App\Users\Contracts;


use App\Users\User;

interface UserRepositoryInterface {
    public function getByInstitution($institution);

    public function approveRegistration($token);

    public function declineRegistration($token);

    public function verify(User $user);

}
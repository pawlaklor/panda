<?php

$path = app_path();

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::group(['middleware' => ['web']], function () use ($path) {
  /*  Route::controllers([
        'auth' => 'Auth\AuthController',
        'password' => 'Auth\PasswordController',
    ]);*/

     Route::auth();

    Route::get('/home', 'HomeController@index');
});

Route::group(['middleware' => ['web', 'auth']], function () use ($path) {

    require_once $path."/Http/Routes/dashboard.php";
    require_once $path."/Http/Routes/users.php";

});
<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Users\Repositories\AccessUserRepository;
use App\Users\User;
use Fuzy\Access\Exceptions\AccessException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Pluralizer;

class DashboardController extends Controller
{

    private $user;

    public function __construct(AccessUserRepository $user)
    {
        $this->user = $user;

        $this->currentUser = Auth::user();
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $users = $this->user->all();

        } catch (AccessException $e) {
            dd($e->getMessage());
            return $this->user->getRedirect();
        }

        return view('dashboard.index', compact('users'));
    }

}

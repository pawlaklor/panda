<?php

namespace App\Http\Controllers\Auth;

use App\Registration\Repositories\AccessRegistrationRepository;
use App\Registration\Requests\RegistrationRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\User;
use App\Users\Repositories\AccessUserRepository;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * @var AccessUserRepository
     */
    private $user;

    /**
     * @var AccessRegistrationRepository
     */
    private $registration;

    /**
     * Create a new authentication controller instance.
     *
     * @param AccessUserRepository         $user
     * @param AccessRegistrationRepository $registration
     */
    public function __construct(
        AccessUserRepository $user,
        AccessRegistrationRepository $registration

    )
    {
        $this->middleware('guest', ['except' => 'logout']);

        $this->user = $user;
        $this->registration = $registration;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        // Not needed

        return Validator::make($data, [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    public function register(RegistrationRequest $request) {

//        dd($request->all());

        // OK, user successfully filled form
        return redirect()->back();
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);



        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        // Add verified = 1 to credentials, so login attempt will check that too.
        $credentials = $this->addVerifiedToLoginAttemptCredentials($credentials);

        if (Auth::guard($this->getGuard())->attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        } else {

            // TODO: Figure out the way for checking if fail was caused by user beeing not verified
            // TODO: We can only check if email exist
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    private function addVerifiedToLoginAttemptCredentials($credentials)
    {
        $credentials['verified'] = 1;

        return $credentials;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('auth.register');
    }


}

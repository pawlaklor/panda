<?php

Route::group(['prefix' => '/'], function () {

    Route::get('/',[
        'as' => 'dashboard.index',
        'uses' => 'DashboardController@index'
    ]);

});
<?php

/**
 * Users
 */
Route::group(['prefix' => 'users'], function () {

    Route::get('/',[
        'as' => 'users.index',
        'uses' => 'UsersController@index'
    ]);

    Route::get('/create',[
        'as' => 'users.create',
        'uses' => 'UsersController@create'
    ]);

    Route::delete('{id}/destroy', [
        'as' => 'users.destroy',
        'uses' => 'UsersController@destroy'
    ]);

    Route::get('/{id}/edit',[
        'as' => 'users.edit',
        'uses' => 'UsersController@edit'
    ]);

    Route::put('/{id}',[
        'as' => 'users.update',
        'uses' => 'UsersController@update'
    ]);

    Route::get('/{id}',[
        'as' => 'users.show',
        'uses' => 'UsersController@show'
    ]);

    Route::post('/',[
        'as' => 'users.store',
        'uses' => 'UsersController@store'
    ]);

});
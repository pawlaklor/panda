<?php namespace App\Registration\Contracts;

use App\Institutions\Institution;

interface RegistrationRepositoryInterface {

    public function createUser(Institution $institution, $data);

    public function confirmEmail($token);
}
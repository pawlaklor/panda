<?php namespace App\Registration\Repositories;


use App\Institutions\Institution;
use App\Registration\Contracts\RegistrationRepositoryInterface;
use App\Users\Repositories\UserRepository;
use App\Users\User;

class RegistrationRepository implements RegistrationRepositoryInterface {

    /**
     * @var UserRepository
     */
    private $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    public function createUser(Institution $institution, $data)
    {
        $data['institution_id'] = $institution->id;

//        $data['password'] = bcrypt($data['password']); // bcrypted in user model with magic method for creating model

        return $this->user->store($data);
    }

    public function confirmEmail($token)
    {
        return $this->user->confirmEmail($token);
    }
}
<?php namespace App\Registration\Repositories;

use App\Institutions\Institution;
use App\Registration\Contracts\RegistrationRepositoryInterface;
use Fuzy\Access\Exceptions\AccessException;
use Fuzy\Access\Exceptions\NotEnoughPermissionsException;
use Fuzy\Access\Exceptions\NotEnoughRolesException;
use Fuzy\Access\Repositories\AccessRepository;

class AccessRegistrationRepository extends AccessRepository implements RegistrationRepositoryInterface {

    protected $access = [

        'functions' => [],
        'redirect' => [
            'route' => 'dashboard.index'
        ]
    ];
    /**
     * @var RegistrationRepository
     */
    private $registration;

    /**
     * @param RegistrationRepository $registration
     */
    public function __construct(RegistrationRepository $registration)
    {

        $this->registration = $registration;
    }


    public function createUser(Institution $institution, $data)
    {
        return $this->registration->createUser($institution, $data);
    }

    public function confirmEmail($token)
    {
        return $this->registration->confirmEmail($token);
    }
}
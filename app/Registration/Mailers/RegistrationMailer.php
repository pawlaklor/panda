<?php namespace App\Registration\Mailers;

use App\Users\User;
use Fuzy\Mailer\HtmlMailer;

class RegistrationMailer extends HtmlMailer {

    public function sendRegistrationDeclinedMessageTo(User $user)
    {
        $subject = config('system.name')." - Registration Declined";
        $view = "auth.emails.registration-declined";

        $consortium = $user->institution()->firstOrFail()->consortium()->get()->first();

        return $this->send($user->email, $subject, $view, compact('user', 'consortium'));
    }

    public function sendRegistrationApprovedMessageTo(User $user)
    {
        $subject = config('system.name')." - Registration Approved";
        $view = "auth.emails.registration-approved";

        $consortium = $user->institution()->firstOrFail()->consortium()->get()->first();

        return $this->send($user->email, $subject, $view, compact('user', 'consortium'));
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function sendAuthorisationEmailAbout(User $user)
    {
        $subject = config('system.name')." - Request for access to HE Contracts";
        $view = "auth.emails.authorisation-email-about-user-registration";

        return $this->send('apapala@fuzy.pl', $subject, $view, compact('user'));
    }

    public function sendEmailConfirmationAfterRegistrationTo(User $user)
    {
        $subject = config('system.name')." - Registration Approved";
        $view = "auth.emails.email-confirmation-after-registration";

        return $this->send($user->email, $subject, $view, compact('user'));
    }

    public function sendConfirmationAfterRegistrationEmailTo($user)
    {
        $subject = "Request - Congrats! You became a Partner!";
        $view = "emails.test";

        return $this->send($user->email, $subject, $view, compact('user'));
    }
}
<?php namespace App\Registration\Providers;

use App\Registration\Repositories\AccessRegistrationRepository;
use App\Registration\Repositories\RegistrationRepository;
use App\Users\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class RegistrationRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->singleton(AccessRegistrationRepository::class, function() {

            $userRepository = new UserRepository();

            $registrationRepository = new RegistrationRepository($userRepository);

            return $accessRegistrationRepository = new AccessRegistrationRepository($registrationRepository);
        });
    }
}

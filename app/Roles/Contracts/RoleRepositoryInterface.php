<?php namespace App\Roles\Contracts;

use App\Users\User;

interface RoleRepositoryInterface {

    public function all();

    public function getUserRoles(User $user);

    public function syncUserRoles(User $user, $data);

}
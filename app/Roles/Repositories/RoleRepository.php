<?php namespace App\Roles\Repositories;

use App\Roles\Contracts\RoleRepositoryInterface;
use App\Users\User;
use App\Users\Role;

class RoleRepository implements RoleRepositoryInterface {

    public function all()
    {
        return Role::all();
    }

    public function getUserRoles(User $user)
    {
        // TODO: Should we really use model provided, not found new model based on $user->id ?

        return array_pluck($user->roles->toArray(), 'id');
    }

    /**
     * @param User $user
     * @param      $data
     * @return array
     */
    public function syncUserRoles(User $user, $data)
    {
        return $user->roles()->sync($data['roles']);
    }
}
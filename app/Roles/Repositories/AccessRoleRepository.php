<?php namespace App\Roles\Repositories;

use App\Roles\Contracts\RoleRepositoryInterface;
use App\Users\User;
use Fuzy\Access\Repositories\AccessRepository;

class AccessRoleRepository extends AccessRepository implements RoleRepositoryInterface {

    /**
     * @var RoleRepository
     */
    private $role;

    protected $access = [

        'functions' => [
            'all' => [
                'roles' => ['asd'],
                'permissions' => []
            ],
            'syncUserRoles' => [
                'roles' => [],
                'permissions' => []
            ]
        ],
        'redirect' => [
            'route' => 'admin.dashboard'
        ]
    ];

    public function __construct(RoleRepository $role)
    {
        $this->role = $role;
    }

    public function all()
    {
        // TODO: Add access of someone who can retrieve all roles in the system

        return $this->role->all();
    }

    public function getUserRoles(User $user)
    {
        // TODO: Add access of someone who can retrieve all roles in the system

        return $this->role->getUserRoles($user);
    }

    public function syncUserRoles(User $user, $data)
    {
        // TODO: Accesss same as all

        return $this->role->syncUserRoles($user, $data);
    }
}
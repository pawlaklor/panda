<?php namespace App\Roles\Providers;

use App\Roles\Contracts\RoleRepositoryInterface;
use App\Roles\Repositories\AccessRoleRepository;
use App\Roles\Repositories\RoleRepository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class RoleRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(RoleRepositoryInterface::class, AccessRoleRepository::class);



        $this->app->singleton(AccessRoleRepository::class, function() {

            $roleRepository = new RoleRepository;

            return $accessRoleRepository = new AccessRoleRepository($roleRepository);

        });


    }
}
